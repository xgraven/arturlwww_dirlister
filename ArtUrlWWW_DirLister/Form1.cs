﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ArtUrlWWW_DirLister
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class Form1 : Form
    {

        public static SQLiteConnection m_dbConnection;

        string webBrowser1Html;

        public Form1()
        {

            InitializeComponent();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            this.Text = this.Text + ", Version " + version;

            var resourceName = "ArtUrlWWW_DirLister.template.html";

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                //webBrowser1Html = reader.ReadToEnd();

                if (!File.Exists("MyDatabase.sqlite"))
                {
                    SQLiteConnection.CreateFile("MyDatabase.sqlite");
                }

                m_dbConnection = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
                m_dbConnection.Open();
                string sql = "create table if not exists objs (" +
                    "`id` INTEGER PRIMARY KEY AUTOINCREMENT, parentID INTEGER, fileName text, parentDirPath text, "
                    + "fileSize INTEGER, dirSize INTEGER DEFAULT 0, lastModifiedDate text, isDir INTEGER, fullPath text, rowDate TEX)";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                sql = "CREATE INDEX if not exists `objs_parentDirPath_idx` ON `objs` (`parentDirPath` ASC)";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                command.Dispose();

                sql = "CREATE INDEX if not exists `objs_rowDate_idx` ON `objs` (`rowDate` ASC)";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                command.Dispose();

                sql = "CREATE INDEX if not exists `objs_parentID_idx` ON `objs` (`parentID` ASC)";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                command.Dispose();

                GetDBDates();
            }
        }

        string prevPath = null;

        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (prevPath != null)
                {
                    fbd.SelectedPath = prevPath;
                }
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK)
                {
                    prevPath = fbd.SelectedPath;
                    // System.Windows.Forms.MessageBox.Show(fbd.SelectedPath);

                    Thread t = new Thread(() => thBtnFunction(fbd.SelectedPath));
                    t.Start();
                }
            }


        }

        public void thBtnFunction(string path)
        {

            string sql = "DELETE FROM objs";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();

            sql = "delete from sqlite_sequence where name='objs';";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();

            SQLiteTransaction transaction = m_dbConnection.BeginTransaction();

            DirIndexer(path, true, 1);
            //DirSearch(@"c:\", true);
            //DirSearch(@"c:\temp", true);

            transaction.Commit();


            /*
              update objs set dirSize = 0;
                        update objs set dirSize = (
                                    WITH RECURSIVE
              works_for_alice(n, m) AS(
               select objs.id, 0
                UNION all
                SELECT a.id, a.fileSize FROM objs a, works_for_alice b
                 WHERE a.parentID = b.n
              )
            SELECT sum(m)FROM works_for_alice
            )

                        */

            sql = "update objs set dirSize = 0;";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();


            sql = @" update objs set dirSize = (
                                    WITH RECURSIVE
                                  works_for_alice(n, m) AS(
                                   select objs.id, 0
                                    UNION all
                                    SELECT a.id, a.fileSize FROM objs a, works_for_alice b
                                     WHERE a.parentID = b.n
                                  )
                                SELECT sum(m)FROM works_for_alice
                                )";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();


            sql = "vacuum";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            command.Dispose();

            try
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    label1.Text = "Done!";

                    dataGridView1.Visible = true;

                    GetDBDates();

                    GetRootDirName("");
                });


            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public void GetRootDirName(string rowDate)
        {
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();

            string sql = "select * from objs where id=1";
            //string sql = "select * from objs where id=1 and rowDate='" + rowDate + "'";
            SQLiteCommand command = new SQLiteCommand(sql, Form1.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            reader.Read();

            string folderName = reader["fileName"].ToString();
            folderName = Path.GetFileName(folderName);

            TreeNode rootNode = new TreeNode(folderName);
            treeView1.Nodes.Add(rootNode);

            rootNode.Tag = 1;
            rootNode.Expand();

            treeView1.EndUpdate();

            reader.Close();
            command.Dispose();

        }



        int dirCounterTmp = 0;
        void DirIndexer(string dirToProcess, bool isRoot, long parentID)
        {

            dirCounterTmp++;
            if (dirCounterTmp > 20)
            {
                dirCounterTmp = 0;
                this.Invoke((MethodInvoker)delegate ()
                {
                    label1.Text = dirToProcess;
                });
            }

            if (isRoot)
            {
                FileInfo di = new FileInfo(dirToProcess);
                string lastModifiedDate = di.LastWriteTime.ToString("dd MM yyyy HH:mm:ss");

                string childDirName1 = dirToProcess.Replace(dirToProcess + @"\", "");

                string sql = "insert into objs (fileName, parentDirPath, fileSize, lastModifiedDate, isDir, fullPath, rowDate) values ('"
                    + childDirName1 + "', '" //currentDirName
                    + "" + "', " //parentDirName
                    + 0 + ", " //fileSize
                    + "'" + lastModifiedDate + "', " //lastModifiedDate
                    + 1 + ", "
                    + "'" + childDirName1 + "', "
                    + "date()"
                    + ")";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
                command.Dispose();
            }

            try
            {
                /*
                 * Lets process files
                 */
                foreach (string f in Directory.GetFiles(dirToProcess))
                {
                    string sql = "";
                    try
                    {
                        //Console.WriteLine(f);
                        string fileName = f.Replace("'", "''");

                        FileInfo fi = new FileInfo(f);
                        string lastModifiedDate = fi.LastWriteTime.ToString("dd MM yyyy HH:mm:ss");

                        fileName = fileName.Replace(dirToProcess + @"\", "");

                        sql = "insert into objs (fileName, parentDirPath, fileSize, lastModifiedDate, isDir, fullPath, " +
                            "rowDate, parentID) values ('"
                              + fileName + "', '" //fileName
                              + dirToProcess.Replace("'", "''") + "', " //parentDirName
                              + fi.Length + ", " //fileSize
                              + "'" + lastModifiedDate + "', " //lastModifiedDate
                              + 0 + ", "
                              + "'" + f.Replace("'", "''") + "', "
                              + "date(), "
                              + parentID
                              + ")";

                        //string sql = "insert into objs (fileName, dirName, fileSize) values ('"
                        //    + fileName + "', '" //fileName
                        //    + parentDir + "', " //dirName
                        //    + fi.Length + ")";
                        SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                        command.ExecuteNonQuery();
                        command.Dispose();
                    }
                    catch (Exception excpt)
                    {
                        Console.WriteLine(sql);
                        Console.WriteLine(excpt.StackTrace);

                    }
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.StackTrace);

            }
            /*
             * End of Lets process files
             */

            try
            {

                foreach (string childDirName in Directory.GetDirectories(dirToProcess))
                {

                    FileInfo di = new FileInfo(childDirName);
                    string lastModifiedDate = di.LastWriteTime.ToString("dd MM yyyy HH:mm:ss");

                    string childDirName1 = childDirName.Replace(dirToProcess + @"\", "");

                    string sql = "insert into objs (fileName, parentDirPath, fileSize, lastModifiedDate, isDir, fullPath, " +
                        "rowDate, parentID) values ('"
                        + childDirName1.Replace("'", "''") + "', '" //currentDirName
                        + dirToProcess.Replace("'", "''") + "', " //parentDirName
                        + 0 + ", " //fileSize
                        + "'" + lastModifiedDate + "', " //lastModifiedDate
                        + 1 + ", "
                        + "'" + childDirName.Replace("'", "''") + "', "
                        + "date(), "
                        + parentID
                        + ")";
                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();
                    command.Dispose();


                    DirIndexer(childDirName, false, m_dbConnection.LastInsertRowId);

                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.StackTrace);
            }
        }


        public string GetDBDates()
        {
            string sql = "select distinct rowDate from objs";

            listBox1.Items.Clear();

            string rowDate = "";
            SQLiteCommand command = new SQLiteCommand(sql, Form1.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                rowDate = reader["rowDate"].ToString();
                listBox1.Items.Add(rowDate);
            }

            return rowDate;

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRootDirName("");
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            GetSubdirs("");
            ListDir("");
        }

        public void GetSubdirs(string rowDate)
        {
            TreeNode selectedNode = treeView1.SelectedNode;
            treeView1.BeginUpdate();
            selectedNode.Nodes.Clear();

            string id = treeView1.SelectedNode.Tag.ToString();
            string sql = "select o.* from objs o inner join objs o2 on o2.id = " + id +
                " and o2.fullPath = o.parentDirPath where o.isDir = 1";

            //string sql = "select o.* from objs o inner join objs o2 on o2.id = " + id +
            //   " and o2.fullPath = o.parentDirPath where o.isDir = 1 and o.rowDate='" + rowDate
            //   + "' and o2.rowDate='" + rowDate + "'";

            SQLiteCommand command = new SQLiteCommand(sql, Form1.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string folderName = reader["fileName"].ToString();
                folderName = Path.GetFileName(folderName);

                TreeNode tn = new TreeNode(folderName);
                tn.Tag = (long)reader["id"];
                tn.ExpandAll();
                selectedNode.Nodes.Add(tn);


                //o.id =;
                //o.fullPath = reader["fullPath"].ToString();
                //o.lastModifiedDate = reader["lastModifiedDate"].ToString();
                //o.dirName = reader["parentDirPath"].ToString();

            }

            reader.Close();
            command.Dispose();

            selectedNode.Expand();
            treeView1.EndUpdate();

        }

        public void ListDir(string rowDate)
        {
            dataGridView1.Rows.Clear();

            TreeNode selectedNode = treeView1.SelectedNode;

            string id = treeView1.SelectedNode.Tag.ToString();

            string sql = "select o.*, " +
                            "case " +
                                 // "when o.isDir = 1 then(select sum(o3.fileSize) from objs o3 where o3.fullPath like o.fullPath || '%' and o3.isDir = 0) " +
                                 "when o.isDir = 1 then o.dirSize " +
                                 "else o.fileSize " +
                            "end as fileSize2 " +
                        "from objs o inner join objs o2 on o2.id = " + id + " and o2.fullPath = o.parentDirPath order by  isDir desc, fileName asc ";

            //string sql = "select o.*, " +
            //                          "case " +
            //                               "when o.isDir = 1 then(select sum(o3.fileSize) from objs o3 where o3.fullPath like o.fullPath || '%' and o3.isDir = 0) " +
            //                               "else o.fileSize " +
            //                          "end as fileSize2 " +
            //                      "from objs o inner join objs o2 on o2.id = " + id + " and o2.fullPath = o.parentDirPath " +
            //                      "where o.rowDate='" + rowDate + "' and o2.rowDate='" + rowDate + "'";

            SQLiteCommand command = new SQLiteCommand(sql, Form1.m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                string folderName = reader["fileName"].ToString();
                folderName = Path.GetFileName(folderName);

                int rowIndex = dataGridView1.Rows.Add();

                //Obtain a reference to the newly created DataGridViewRow 
                var row = dataGridView1.Rows[rowIndex];

                row.Cells[0].Value = reader["fileName"];//(long)reader["id"];

                object x = reader["fileSize2"];
                if (x.GetType().Name.Equals("DBNull"))
                {
                    row.Cells[1].Value = -1;
                }
                else
                {
                    row.Cells[1].Value = (long)reader["fileSize2"];
                }

                row.Tag= (long)reader["id"];

                //o.isDir = (long)reader["isDir"];
                //o.fullPath = reader["fullPath"].ToString();
                //o.lastModifiedDate = reader["lastModifiedDate"].ToString();
                //o.dirName = reader["parentDirPath"].ToString();

            }

        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //dataGridView1.Rows[e.RowIndex];
        }
    }
    //End of class
}
//End of namespace